# rgit
`rgit` scans the directory tree under the current working directory for GIT directories. It will then execute the specified GIT command there.

*WARNING: you can really mess up things if you use rgit the wrong way.*

# Usage
Commands are the same as for `git`. Just type `rgit` instead of `git`.

## Example
Show `git status` in all GIT directiories within `$HOME`
```sh
cd $HOME
rgit status
```

## Running multiple git commands in parallel
If `parallel` is installed on your system then `rgit` will run many git commands at the same time. This significantly speeds up fetching of a large collection of repos.
